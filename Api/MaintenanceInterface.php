<?php
/**
 * @author Andra Lungu <andra.lungu@bitbull.it>
 */
namespace Bitbull\DeployAdvanced\Api;

/**
 * Interface MaintenanceInterface
 * @package Bitbull\DeployAdvanced\Api
 * @api
 */
interface MaintenanceInterface
{

    /**
     * @param bool $mode
     * @param string[] $addresses
     * @return bool
     */
    public function setMaintenance($mode = false, $addresses = []);
}