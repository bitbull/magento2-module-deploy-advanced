# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/). 

# [2.3.2] - 2020-12-11
- Fixed check of HTTP_X_FORWARDED_FOR that can contain multiple ips
- Performed refactoring to add tests
- Added unit tests

# [2.3.1] - 2020‑06‑23
- Updated documentation

# [2.3.0] - 2020-06-08
- Add PHP 7.2/7.3 compatibility
- Update Magento core dependency
- Reverse order of entries in this file (most recent on top)

# [2.2.0] - 2019-09-10
- add Php 7.1 compatibility
- update core dependency
- Ref. ECM-181 Add missing menu definition

# [2.1.0] - 2018-05-24
- added feature for maintenance in shared folder for multi server infrastructure
- added setup upgrade optimization run only when needed so only once in a multi server infrastructure with maintenance on/off included.

## [1.0.0] - 2018-04-23
- fix for magento issue #8774 caused by recursive read of the folders in efs 
-https://www.maxpronko.com/blog/improving-performance-when-upgrading-magento-2-elastic-file-system
