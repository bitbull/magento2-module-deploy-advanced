<?php
namespace Bitbull\DeployAdvanced\Console\Command;

/**
 * Command for disabling maintenance mode
 */
class MaintenanceDisableCommand extends \Magento\Backend\Console\Command\AbstractMaintenanceCommand
{
    /**
     * Initialization of the command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('bitbull:maintenance:disable')
            ->setDescription('Disables maintenance mode');
        parent::configure();
    }

    /**
     * Disable maintenance mode
     *
     * @return bool
     */
    protected function isEnable()
    {
        return false;
    }

    /**
     * Get disabled maintenance mode display string
     *
     * @return string
     */
    protected function getDisplayString()
    {
        return '<info>Bitbull disabled maintenance mode</info>';
    }

    /**
     * Return if IP addresses effective for maintenance mode were set
     *
     * @return bool
     */
    public function isSetAddressInfo()
    {
        return count($this->maintenanceMode->getAddressInfo()) > 0;
    }
}
