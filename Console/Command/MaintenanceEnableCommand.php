<?php

namespace Bitbull\DeployAdvanced\Console\Command;
/**
 * MaintenanceEnableCommand
 *
 * @copyright Copyright © 2018 Bitbull. All rights reserved.
 * @author    andra.lungu@bitbull.it
 */

class MaintenanceEnableCommand extends \Magento\Backend\Console\Command\AbstractMaintenanceCommand
{

    /**
     * Initialization of the command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('bitbull:maintenance:enable')
            ->setDescription('Bitbull enables maintenance mode');
        parent::configure();
    }

    /**
     * Enable maintenance mode
     *
     * @return bool
     */
    protected function isEnable()
    {
        return true;
    }

    /**
     * Get enabled maintenance mode display string
     *
     * @return string
     */
    protected function getDisplayString()
    {
        return '<info>Bitbull Enabled maintenance mode</info>';
    }
}