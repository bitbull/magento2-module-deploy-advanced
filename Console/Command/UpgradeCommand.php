<?php

namespace Bitbull\DeployAdvanced\Console\Command;

use Magento\Framework\App\MaintenanceMode;
use Magento\Framework\Module\DbVersionInfo;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for updating installed application after the code base has changed
 */
class UpgradeCommand extends \Magento\Setup\Console\Command\AbstractSetupCommand
{
    /**
     * Option to skip deletion of var/generation directory
     */
    const INPUT_KEY_KEEP_GENERATED = 'keep-generated';

    /**
     * @var DbVersionInfo $dbVersionInfo
     */
    private $dbVersionInfo;

    /**
     * Handler for maintenance mode
     *
     * @var MaintenanceMode
     */
    private $maintenanceMode;

    /**
     * Constructor
     * @param MaintenanceMode $maintenanceMode
     * @param DbVersionInfo $dbVersionInfo
     */
    public function __construct(MaintenanceMode $maintenanceMode, DbVersionInfo $dbVersionInfo)
    {
        $this->dbVersionInfo = $dbVersionInfo;
        $this->maintenanceMode = $maintenanceMode;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $options = [
            new InputOption(
                self::INPUT_KEY_KEEP_GENERATED,
                null,
                InputOption::VALUE_NONE,
                'Prevents generated files from being deleted. ' . PHP_EOL .
                'We discourage using this option except when deploying to production. ' . PHP_EOL .
                'Consult your system integrator or administrator for more information.'
            )
        ];
        $this->setName('bitbull:setup:upgrade')
            ->setDescription('Upgrades the Magento application, DB data, and schema')
            ->setDefinition($options);
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $keepGenerated = $input->getOption(self::INPUT_KEY_KEEP_GENERATED);
        $dbVersionErros = $this->dbVersionInfo->getDbVersionErrors();
        if (count($dbVersionErros) > 0) {
            $this->maintenanceMode->set(true);
            $output->writeln('<info>Maintenance on.</info>');
            $this->call('setup:upgrade', ['--' . self::INPUT_KEY_KEEP_GENERATED => $keepGenerated], $output);
            $this->maintenanceMode->set(false);
            $output->writeln('<info>Maintenance off.</info>');
        }else {
            $output->writeln('<info>No upgrade needed.</info>');
        }
        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }

    protected function call($name, $arguments, OutputInterface $output)
    {
        $command = $this->getApplication()->find($name);
        $arguments = array_merge(['command' => $name], $arguments);
        return $command->run(new ArrayInput($arguments), $output);
    }
}
