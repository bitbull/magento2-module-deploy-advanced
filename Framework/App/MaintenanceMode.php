<?php

namespace Bitbull\DeployAdvanced\Framework\App;

use Magento\Framework\Filesystem;
use Bitbull\DeployAdvanced\Helper\Data as Helper;

/**
 * Application Maintenance Mode
 */
class MaintenanceMode extends \Magento\Framework\App\MaintenanceMode
{
    const FLAG_FILENAME = '.maintenance.flag';

    const MAINTENANCE_URI = 'V1/maintenance';

    const IP_FILENAME = '.maintenance.ip';

    const SHARED = 'shared';

    /**
     * @var string
     */
    private $baseAbsolutePath;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\Filesystem $filesystem
     * @param Helper $helper
     */
    public function __construct(Filesystem $filesystem, Helper $helper)
    {
        parent::__construct($filesystem);
        $this->helper = $helper;
        $baseAbsolutePath = $this->flagDir->getAbsolutePath();
        $sharedPath = $this->helper->getBitbullMaintenanceSharedPath() ?: self::SHARED;
        $this->baseAbsolutePath = $baseAbsolutePath . $sharedPath . DIRECTORY_SEPARATOR;
    }


    /**
     * Checks whether mode is on
     *
     * Optionally specify an IP-address to compare against the white list
     *
     * @param string $originalClientIp
     * @return bool
     */
    public function isOn($originalClientIp = '')
    {
        if (!$this->helper->getIsEnabledBitbullMaintenance()) {
            return parent::isOn($originalClientIp);
        }

        if (!$this->helper->maintenanceFileExists($this->baseAbsolutePath)) {
            return false;
        }

        if (!$this->helper->whitelistFileExists($this->baseAbsolutePath)
            || empty($this->helper->getWhitelistFileContent($this->baseAbsolutePath))) {
            return true;
        }

        // Reference: https://en.wikipedia.org/wiki/X-Forwarded-For
        $httpXForwardedFor = $this->helper->getHttpXForwardedFor();
        if (!empty($httpXForwardedFor)) {
            $allRequestingIps = explode(',', $httpXForwardedFor);
            $originalClientIp = trim($allRequestingIps[0]);
        }

        return !in_array($originalClientIp, $this->getAddressInfo());
    }

    /**
     * Get list of IP addresses effective for maintenance mode
     *
     * @return string[]
     */
    public function getAddressInfo()
    {
        if (!$this->helper->getIsEnabledBitbullMaintenance()) {
            return parent::getAddressInfo();
        }
        if ($this->helper->whitelistFileExists($this->baseAbsolutePath)) {
            $temp = $this->helper->getWhitelistFileContent($this->baseAbsolutePath);
            return array_filter(array_map('trim', explode(',', $temp)));
        } else {
            return [];
        }
    }

    /**
     * Sets maintenance mode "on" or "off"
     *
     * @param bool $isOn
     * @return bool
     */
    public function set($isOn)
    {
        if (!$this->helper->getIsEnabledBitbullMaintenance()) {
            return parent::set($isOn);
        }
        if ($isOn) {
            return touch($this->baseAbsolutePath . self::FLAG_FILENAME);
        }
        if (file_exists($this->baseAbsolutePath . self::FLAG_FILENAME)) {
            return unlink($this->baseAbsolutePath . self::FLAG_FILENAME);
        }
        return true;
    }

    /**
     * Sets list of allowed IP addresses
     *
     * @param string $addresses
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function setAddresses($addresses)
    {
        if (!$this->helper->getIsEnabledBitbullMaintenance()) {
            return parent::setAddresses($addresses);
        }
        $addresses = (string)$addresses;
        if (empty($addresses)) {
            if (file_exists($this->baseAbsolutePath . self::IP_FILENAME)) {
                return unlink($this->baseAbsolutePath . self::IP_FILENAME);
            }
            return true;
        }
        if (!preg_match('/^[^\s,]+(,[^\s,]+)*$/', $addresses)) {
            throw new \InvalidArgumentException("One or more IP-addresses is expected (comma-separated)\n");
        }
        $result = file_put_contents($this->baseAbsolutePath . self::IP_FILENAME, $addresses);
        return false !== $result ? true : false;
    }
}
