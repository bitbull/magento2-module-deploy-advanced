<?php
/**
 * Data
 *
 * @copyright Copyright © 2017 Bitbull. All rights reserved.
 * @author    andra.lungu@bitbull.it
 */

namespace Bitbull\DeployAdvanced\Helper;

use Bitbull\DeployAdvanced\Framework\App\MaintenanceMode;
use Magento\Framework\App\Helper\AbstractHelper;


class Data extends AbstractHelper
{
    const XML_BITBULL_MAINTENANCE_ENABLED = 'bitbull_maintenance/general/enable';
    const XML_BITBULL_MAINTENANCE_SHARED_PATH = 'bitbull_maintenance/general/shared_path';

    /**
     * Check if importer is enabled
     *
     * @return int
     */
    public function getIsEnabledBitbullMaintenance()
    {
        return (int)$this->scopeConfig->getValue(
            self::XML_BITBULL_MAINTENANCE_ENABLED);
    }

    /**
     * get shared path where to check for maintenance file
     *
     * @return string
     */
    public function getBitbullMaintenanceSharedPath()
    {
        return $this->scopeConfig->getValue(
            self::XML_BITBULL_MAINTENANCE_SHARED_PATH);
    }

    public function maintenanceFileExists($filepath)
    {
        return file_exists($filepath . MaintenanceMode::FLAG_FILENAME);
    }

    public function whitelistFileExists($filepath)
    {
        return file_exists($filepath . MaintenanceMode::IP_FILENAME);
    }

    public function getWhitelistFileContent($filepath)
    {
        return file_get_contents($filepath . MaintenanceMode::IP_FILENAME);
    }

    public function getHttpXForwardedFor()
    {
        return $_SERVER['HTTP_X_FORWARDED_FOR'] ?? '';
    }
}
