<?php
/**
 * Maintenance
 *
 * @copyright Copyright © 2018 Bitbull. All rights reserved.
 * @author    andra.lungu@bitbull.it
 */
namespace  Bitbull\DeployAdvanced\Model;
use Bitbull\DeployAdvanced\Api\MaintenanceInterface;
use Magento\Framework\App\MaintenanceMode;
use Bitbull\DeployAdvanced\Helper\Data as Helper;

class Maintenance implements MaintenanceInterface
{

    /**
     * Handler for maintenance mode
     *
     * @var MaintenanceMode
     */
    private $maintenanceMode;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * Constructor
     *
     * @param MaintenanceMode $maintenanceMode
     * @param Helper $helper
     */
    public function __construct(MaintenanceMode $maintenanceMode, Helper $helper)
    {
        $this->maintenanceMode = $maintenanceMode;
        $this->helper = $helper;
    }

    /**
     * @param bool $mode
     * @param array $addresses
     * @return bool
     */
    public function setMaintenance($mode = false, $addresses = [])
    {
        if(!$this->helper->getIsEnabledBitbullMaintenance())
        {
            return false;
        }

        $stringAddresses = '';
        if(count($addresses) > 0 && $mode) {
            $stringAddresses = implode(',', $addresses);
        }
        $this->maintenanceMode->setAddresses($stringAddresses);
        return $this->maintenanceMode->set($mode);
    }
}