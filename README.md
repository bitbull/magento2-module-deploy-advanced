# Advanced Deploy for Magento 2
This module is an utility for Magento 2 Deploy containing fixes and improvements

## Description
- Fixes nfs recursive read of files and folders under pub during deploy, more specifically during setup:upgrade

- Changes the default path of where to write/read the maintenance files in order to put in maintenance a multi server environment in one shot by adding a specific api that maintenance mode won't disable. Added also custom commands for maintenance since the default one is in the setup folder and therefore not using the preferences defined in our module.

- Cli command that runs setup:upgrade only if needed so in a multiserver environment will be run only when needed (if either setup or data version change) and only once.

## Settings
- enable: if is enabled the custom maintenance will be used, default otherwise
- shared-path: folder location where to look for the maintenance files that could be a symlinked folder in a shared location ( example var/shared/ )

## Running tests
To run the unit tests, from Magento's root directory within your container launch:

    vendor/bin/phpunit -c dev/tests/unit/phpunit.xml.dist vendor/bitbull/magento2-module-deploy-advanced/Test/Unit/

