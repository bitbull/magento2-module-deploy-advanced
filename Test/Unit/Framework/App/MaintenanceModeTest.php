<?php declare(strict_types=1);

namespace Bitbull\DeployAdvanced\Test\Unit\Framework\App;

use Bitbull\DeployAdvanced\Framework\App\MaintenanceMode;
use Bitbull\DeployAdvanced\Helper\Data;
use Magento\Framework\Filesystem;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MaintenanceModeTest extends TestCase
{
    /**
     * @var Filesystem | MockObject
     */
    private $filesystem;

    /**
     * @var Data | MockObject
     */
    private $helper;

    /**
     * @var MaintenanceMode
     */
    private $maintenanceMode;

    /**
     * setup tests
     */
    protected function setUp(): void
    {
        $writeInterface = $this->createMock(\Magento\Framework\Filesystem\Directory\WriteInterface::class);

        $this->filesystem = $this->createMock(Filesystem::class);
        $this->filesystem
            ->expects($this->any())
            ->method('getDirectoryWrite')
            ->willReturn($writeInterface);

        $this->helper = $this->createMock(Data::class);

        $this->maintenanceMode = new MaintenanceMode(
            $this->filesystem,
            $this->helper
        );
    }

    /**
     * @covers \Bitbull\DeployAdvanced\Framework\App\MaintenanceMode::isOn
     * @covers \Bitbull\DeployAdvanced\Framework\App\MaintenanceMode::__construct
     */
    public function testIsOnWithoutIpWhitelistFile()
    {
        $this->helper
            ->expects($this->any())
            ->method('getIsEnabledBitbullMaintenance')
            ->willReturn(true);

        $this->helper
            ->expects($this->once())
            ->method('maintenanceFileExists')
            ->willReturn(true);

        $this->helper
            ->expects($this->once())
            ->method('whitelistFileExists')
            ->willReturn(false);

        $this->assertTrue($this->maintenanceMode->isOn());
    }

    /**
     * @covers       \Bitbull\DeployAdvanced\Framework\App\MaintenanceMode::isOn
     * @covers       \Bitbull\DeployAdvanced\Framework\App\MaintenanceMode::__construct
     * @dataProvider whitelistData()
     */
    public function testIsOnWithIpWhitelistFileWithoutForward($fileContent, $originalClientIp, $expectedResult)
    {
        $this->helper
            ->method('getIsEnabledBitbullMaintenance')
            ->willReturn(true);

        $this->helper
            ->method('maintenanceFileExists')
            ->willReturn(true);

        $this->helper
            ->method('whitelistFileExists')
            ->willReturn(true);

        $this->helper
            ->method('getWhitelistFileContent')
            ->willReturn($fileContent);

        $this->assertSame($expectedResult, $this->maintenanceMode->isOn($originalClientIp));
    }

    public function whitelistData()
    {
        return [
            'with empty file' => ['', '192.168.1.1', true],
            'with non empty file' => ['192.168.1.100', '192.168.1.1', true],
            'with non empty file matching mine' => ['192.168.1.1', '192.168.1.1', false],
            'with non empty multiple ips file' => ['192.168.1.100, 192.168.1.101, ', '192.168.1.1', true],
            'with non empty multiple ips file matching mine' => ['192.168.1.100, 192.168.1.1, ', '192.168.1.1', false],
        ];
    }

    /**
     * @covers       \Bitbull\DeployAdvanced\Framework\App\MaintenanceMode::isOn
     * @covers       \Bitbull\DeployAdvanced\Framework\App\MaintenanceMode::__construct
     * @dataProvider whitelistDataWithForward()
     */
    public function testIsOnWithIpWhitelistFileWithForward($fileContent, $originalClientIp, $httpXForwardedFor, $expectedResult)
    {
        $this->helper
            ->method('getIsEnabledBitbullMaintenance')
            ->willReturn(true);

        $this->helper
            ->method('maintenanceFileExists')
            ->willReturn(true);

        $this->helper
            ->method('whitelistFileExists')
            ->willReturn(true);

        $this->helper
            ->method('getWhitelistFileContent')
            ->willReturn($fileContent);

        $this->helper
            ->method('getHttpXForwardedFor')
            ->willReturn($httpXForwardedFor);

        $this->assertSame($expectedResult, $this->maintenanceMode->isOn($originalClientIp));
    }


    public function whitelistDataWithForward()
    {
        return [
            'with empty file and single forward' => ['', '192.168.1.1', '192.168.1.10', true],
            'with empty file and multiple forward' => ['', '192.168.1.1', '192.168.1.10, 192.168.1.11', true],

            'with non empty file and single forward' => ['192.168.1.100', '192.168.1.1', '192.168.1.10', true],
            'with non empty file and multiple forward' => ['192.168.1.100', '192.168.1.1', '192.168.1.10, 192.168.1.11', true],

            'with non empty file matching mine and single forward' => ['192.168.1.11', '192.168.1.1', '192.168.1.11', false],
            'with non empty file matching mine and multiple forward' => ['192.168.1.10', '192.168.1.1', '192.168.1.10, 192.168.1.11', false],
            'with non empty file not matching mine and multiple forward' => ['192.168.1.11', '192.168.1.1', '192.168.1.10, 192.168.1.11', true],

            'with non empty multiple ips file matching mine and single forward' => ['192.168.1.12, 192.168.1.11', '192.168.1.1', '192.168.1.11', false],
            'with non empty multiple ips file matching mine and multiple forward' => ['192.168.1.12, 192.168.1.10', '192.168.1.1', '192.168.1.10, 192.168.1.11', false],
            'with non empty multiple ips file not matching mine and multiple forward' => ['192.168.1.12, 192.168.1.11', '192.168.1.1', '192.168.1.10, 192.168.1.11', true],
        ];
    }
}
