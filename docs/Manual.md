# Deploy Advanced for Magento 2

## Maintenance

The following commands extend the default behaviour by changing the default location of maintenance files in order to share it between servers if `bitbull_maintenance/general/enable` is true, otherwise default behaviour is in place. 

The default folder in case `bitbull_maintenance/general/enable` is true, is var/shared, otherwise the one specific in `bitbull_maintenance/general/shared_path` relative to var folder.
**Please note**: it assumes the directory already exists, won't work otherwise.

    bitbull:maintenance:allow-ips

same as `maintenance:allow-ips` except that if bitbull_maintenance/general/enable is true, the mainteinance ip file will be written and read from a shared directory

    bitbull:maintenance:disable
 
same as `maintenance:disable` except that if bitbull_maintenance/general/enable is true, the maintenance file will be written and read from a shared directory

    bitbull:maintenance:enable

same as `maintenance:enable` except that if bitbull_maintenance/general/enable is true, the maintenance file will be written and read from a shared directory

    bitbull:maintenance:status

same as `maintenance:status` except that if bitbull_maintenance/general/enable is true, the maintenance file will be written and read from a shared directory
 
## Change maintenance status via API
 
Sample request to enable maintenance with the possibility for allowed ips to access the website ( at least the IP of the server calling the api must be added to be able to disable later the maintenance otherwise only possibility left is from cli):

    POST http://goldenpoint-msite.bitbull/rest/V1/maintenance/1
    Accept: */*
    Cache-Control: no-cache
    Content-Type: application/json
    Authorization: Bearer {token}
    
    {"addresses":["127.0.0.1"]}

Sample request to disable maintenance via api

    POST http://goldenpoint-msite.bitbull/rest/V1/maintenance/0
    Accept: */*
    Cache-Control: no-cache
    Content-Type: application/json
    Authorization: Bearer {token}

## Upgrade

    bitbull:setup:upgrade

same as setup:upgrade except that it checkes if an upgrade is need and if needs, puts in maintenance, runs the upgrade, disables the maintenance. This way it runs only when needed and can be avoided for when the infrastructure scales up.
